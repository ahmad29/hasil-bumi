﻿<?php                   // Memulai session
include 'navbar.php';                     // Panggil koneksi ke database
?>

<br><br><br><br><br><br><br>
<?php if(isset( $_POST['cari'] )){ ?>
    <?php
$keyword   = mysqli_real_escape_string($conn, $_POST['keyword']);
$id_kategori       = mysqli_real_escape_string($conn, $_POST['id_kategori']);
// Memanggil data dari tabel produk diurutkan dengan id_produk secara DESC dan dibatasi sesuai $start dan $per_halaman
$data     = mysqli_query($conn, "SELECT * FROM tb_hasil_pertanian a 
                    JOIN tb_kategori b ON a.id_kategori = b.id_kategori 
                    JOIN tb_petani c ON a.id_petani = c.id_petani 
                    JOIN tb_alamat d ON c.id_alamat = d.id_alamat WHERE a.id_kategori = $id_kategori AND d.alamat like '%".$keyword."%' ORDER BY a.id_hasil_pertanian DESC");
$numrows  = mysqli_num_rows($data);
?>
<section class="listing-products grid-3 list" data-listing-id="listing-1">
<div class="container">
<div class="row">
<div class="col-12">
<div class="section-header text-center">
<h3>Search Result '<?php echo $keyword; ?>' </h3>
</div>
</div>
</div>
<div class="row">


<?php
// Jika data ketemu, maka akan ditampilkan dengan While
if($numrows > 0)
{
  while($row = mysqli_fetch_assoc($data))
  {
    
?>
            <!-- awal -->
        <div class="col-12 col-md-6 col-lg-4 item">
        <div class="item-wrapper">
        <div class="image-wrapper">
        <a href="view-detail.php?id=<?php echo $row['id_hasil_pertanian']; ?>" class="image">
        <?php if( $row['nama_foto'] == NULL){ ?>
            <img class="" src="gambar/no-image.png" data-src="#" alt=""> 
        <?php }else{ ?>
            <img class="" src="gambar/<?php echo $row['nama_foto']; ?>" data-src="#" alt="">
        <?php } ?>
        </a>
        <div class="quick-actions">
        <a href="view-detail.php?id=<?php echo $row['id_hasil_pertanian']; ?>" data-toggle="modal" class="btn btn-primary btn-circle"><i class="fa fa-eye" aria-hidden="true"></i></a>
        <a href="view-detail.php?id=<?php echo $row['id_hasil_pertanian']; ?>" class="btn btn-primary btn-circle add-to-fav"><i class="fa fa-heart" aria-hidden="true"></i></a>
        </div>
        </div>
        <div class="info-wrapper">
        <div class="info">
        <div class="category"><?php echo $row['nama_kategori']; ?></div>
        <a href="view-detail.php?id=<?php echo $row['id_hasil_pertanian']; ?>" class="name"><?php echo $row['nama_hasil_perkebunan']; ?></a>
        <span class="price"><i class="far fa-clock" aria-hidden="true"></i> &nbsp; <?php echo $row['tanggal_post']; ?></span>
        <span class="excerpt"><?php echo $row['deskripsi']; ?></span>
        <span class="location"><i class="fa fa-map-marker-alt" aria-hidden="true"></i> &nbsp; <?php echo $row['alamat']; ?></span>
        
        </div>
        </div>
        </div>
        </div>
        <!-- akhir -->
  
  
  <?php
  // Mengakhiri pengulangan while
  }
}else{ ?>
        <!-- awal -->
        <div class="col-12 col-md-6 col-lg-4 item">
        <div class="item-wrapper">
        <div class="image-wrapper">
        <a href="#" class="image">
        <img class="lazyloaded" src="gambar/outofstock.jpg" data-src="#" alt="">
        </a>
        
        </div>
        <div class="info-wrapper">
        <div class="info">
        <div class="category">Maaf , Hasil Perkebunan Yang Anda Cari Tidak Di temukan</div>
        <a href="index.php" class="name">Back To Home</a>
        
        </div>
        </div>
        </div>
        </div>
        <!-- akhir -->
<?php
}

?>
 <?php } ?>
   
</section>
</div>

 <?php                   // Memulai session
include 'footer.php';                     // Panggil koneksi ke database
?>

