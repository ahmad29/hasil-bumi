﻿<?php                   // Memulai session
include 'navbar.php';                     // Panggil koneksi ke database
?>

<br><br><br><br><br><br><br>
<?php if(isset( $_GET['id'] )){ ?>
    <?php

// Memanggil data dari tabel produk diurutkan dengan id_produk secara DESC dan dibatasi sesuai $start dan $per_halaman
$data     = mysqli_query($conn, "SELECT * FROM tb_hasil_pertanian a 
                    JOIN tb_kategori b ON a.id_kategori = b.id_kategori 
                    JOIN tb_petani c ON a.id_petani = c.id_petani 
                    JOIN tb_alamat d ON c.id_alamat = d.id_alamat WHERE a.id_hasil_pertanian = $_GET[id] ");
$numrows  = mysqli_num_rows($data);
?>

<?php
// Jika data ketemu, maka akan ditampilkan dengan While
if($numrows > 0)
{
  while($row = mysqli_fetch_assoc($data))
  {
    
?>

<div class="content">
<section class="bkg-light view-listing-wrapper">
<div class="container">
<div class="row">
<div class="col-12">
<div class="view-listing">


<div class="title">
<div class="row justify-content-center align-self-center h-100">
<div class="col-12 col-lg-12 my-auto">
<div class="row justify-content-center align-self-center h-100">
<div class="col-12 col-md-8 my-auto">
<h4><?php echo $row['nama_hasil_perkebunan']; ?></h4>
</div>
<div class="col-12 col-md-4 my-auto">
<div class="price"><i class="far fa-clock" aria-hidden="true"></i> <?php echo $row['tanggal_post']; ?></div>
</div>
</div>
</div>

</div>
</div>
<div class="infos">
<div class="row">
<div class="col-12 col-lg-8">
<div class="gallery">

<div class="slider-single animation-slide height-auto dir-nav">
<div class="slider-item">
<?php if( $row['nama_foto'] == NULL){ ?>
            <div class="image" style="background-image: url('gambar/no-photo.png');">
            <a href="gambar/no-photo.png" data-toggle="lightbox" data-gallery="product-gallery"><img src="<?php echo $base_url ?>assets/images/products/audi-1.jpg" alt="" /></a>
            </div>
        <?php }else{ ?>
            <div class="image" style="background-image: url('gambar/<?php echo $row['nama_foto']; ?>');">
            <a href="gambar/<?php echo $row['nama_foto']; ?>" data-toggle="lightbox" data-gallery="product-gallery"><img src="<?php echo $base_url ?>assets/images/products/audi-1.jpg" alt="" /></a>
            </div>
        <?php } ?>

</div>
</div>
</div>
</div>
<div class="col-12 col-lg-4">
<div class="actions-block">
<ul class="stack">
<li>
<span class="user-details-wrapper">
<span class="user-details">
<span class="user-img">
<span class="avatar text-center">
<img src="gambar/user.jpg">
</span>
</span>
<span class="user-name">
<span class="name">
<?php echo $row['nama_petani']; ?>
<span class="verified"><i class="far fa-check-circle"></i> Verified</span>
</span>
</span>
</span>
</span>
</li>
<li>
<span class="joined">
<strong>Joined:</strong> <?php echo $row['tanggal_registrasi']; ?>
</span>
</li>
<li>
<a href="https://wa.me/62<?php echo $row['no_hp']; ?>?text=Saya%20Berminat%20dengan%20<?php echo $row['nama_hasil_perkebunan'];?>%20<?php echo $row['deskripsi']; ?>" target="_BLANK" class="btn btn-primary"><img class="" src="gambar/wa.png" width="20"> Whatsapp</a>
<a href="#" class="btn btn-primary"><i class="fa fa-phone"></i> 0<?php echo $row['no_hp']; ?></a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="details">
<div class="row justify-content-center">
<div class="col-12">
<div class="listing-custom labels">
<div class="item labeled">
<span>Kategori </span>
<span><?php echo $row['nama_kategori']; ?></span>
</div>
<div class="item labeled">
<span>Deskripsi </span>
<span><?php echo $row['deskripsi']; ?></span>
</div>
<div class="item labeled">
<span>Luas Lahan </span>
<span><?php echo $row['luas_lahan']; ?> H</span>
</div>
<div class="item labeled">
<span>Hasil Lahan </span>
<span><?php echo $row['hasil_per_lahan']; ?> TON</span>
</div>
<div class="item labeled">
<span>Panen </span>
<span><?php echo $row['panen_permusim']; ?> Kali / Tahun</span>
</div>
<div class="item labeled">
<span>Pendapatan </span>
<span>IDR <?php echo $row['pendapatan_lahan']; ?></span>
</div>

</div>

</div>
</div>
</div>
<div class="row">
<div class="col-12">


</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

</div>
<?php
}
}

?>
 <?php } ?>

 <?php                   // Memulai session
include 'footer.php';                     // Panggil koneksi ke database
?>

