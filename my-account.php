﻿<?php                   // Memulai session
include 'navbar.php';                     // Panggil koneksi ke database
?>

<br><br><br><br><br><br><br>


<div class="content">
<section class="bkg-light medium boxed">
<div class="container">
<div class="row">
<div class="col-12">
<div class="boxed-container my-account">
<div class="account-nav">
<ul class="subnav justify-content-center align-self-center h-100">
<li class="my-auto "><a href="my-post.php">My Post</a></li>
<li class="my-auto active"><a href="my-account.php">My Account</a></li>
</ul>
</div>
<div class="account-nav-mobile">
<ul>
<li class=""><a href="javascript:;" data-toggle="collapse" data-target="#menu-options" aria-expanded="false" class="collapsed">My Ads</a></li>
<li class=""><a href="javascript:;" data-toggle="collapse" data-target="#menu-options" aria-expanded="false" class="collapsed">Favorite Ads</a></li>

</ul>
<div id="menu-options" class="menu-options collapse" aria-expanded="false">
<ul>
<li class=""><a href="#">My Ads</a></li>
<li class=""><a href="#">Favorite Ads</a></li>
</ul>
</div>
</div>
<div class="account-block">
<div class="row justify-content-center">
<div class="col-12">
<div class="section-header text-center">
<h3 class="d-none d-lg-block">My Account</h3>
</div>
</div>
<?php
$id_petani = $_SESSION['id_petani'];
// Memanggil data dari tabel produk diurutkan dengan id_produk secara DESC dan dibatasi sesuai $start dan $per_halaman
$data     = mysqli_query($conn, "SELECT * FROM tb_petani a JOIN tb_alamat b ON a.id_alamat = b.id_alamat WHERE a.id_petani = $id_petani ");
$numrows  = mysqli_num_rows($data);
?>

<?php
// Jika data ketemu, maka akan ditampilkan dengan While
if($numrows > 0)
{
  while($row = mysqli_fetch_assoc($data))
  {
    
?>
<div class="col-12 col-lg-10">

<div class="row justify-content-center">
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" placeholder="First Name" value="Nama : <?php echo $row['nama_petani']; ?>" readonly>
</div>
</div>
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" placeholder="Last Name" value="Email : <?php echo $row['email']; ?>" readonly>
</div>
</div>
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" placeholder="Last Name" value="Username : <?php echo $row['username']; ?>" readonly>
</div>
</div>
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" placeholder="Last Name" value="Password : <?php echo $row['password']; ?>" readonly>
</div>
</div>
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" placeholder="Last Name" value="Alamat : <?php echo $row['alamat']; ?>" readonly>
</div>
</div>
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" placeholder="Last Name" value="No HP : <?php echo $row['no_hp']; ?>" readonly>
</div>
</div>
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" placeholder="Last Name" value="No KTP : <?php echo $row['no_ktp']; ?>" readonly>
</div>
</div>
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" placeholder="Last Name" value="Tanggal Registrasi : <?php echo $row['tanggal_registrasi']; ?>" readonly>
</div>
</div>

<?php
  // Mengakhiri pengulangan while
  }
}

?>
</div>

</div>
</div>
</div>

</div>
</section>
</div>


 <?php                   // Memulai session
include 'footer.php';                     // Panggil koneksi ke database
?>

