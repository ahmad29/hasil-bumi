<?php                   // Memulai session
include 'navbar.php';                     // Panggil koneksi ke database
?>
<br><br><br><br><br><br>
<div class="content">
<section class="bkg-light medium">
<div class="container">
<div class="row">
<div class="col-12">
<div class="boxed-container">
<div class="block">
<div class="row">
<div class="col-12">
<div class="section-header text-center">
<h3>ini adalah halaman about</h3>
<span class="caption-header">Lorem Ipsum is simply dummy text. </span>
</div>
</div>
</div>
<div class="row">
<div class="col-12">
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop
publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</p>
<h4>SUBHEADING</h4>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a
more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and
web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have
evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
</p>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
<li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
<li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
et quasi architecto beatae vitae dicta sunt explicabo.</li>
</ul>
<blockquote>
At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi
sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.
It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a
more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
</blockquote>
</div>
<div class="col-12 col-lg-6">
<h4>SUBHEADING</h4>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a
more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and
web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have
evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
</p>
</div>
<div class="col-12 col-lg-6">
<h4>SUBHEADING</h4>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.</li>
<li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
utaliquipex ea commodo consequat.</li>
<li>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
eu fugiat nulla pariatur.</li>
<li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
doloet quasi architecto beatae vitae dicta sunt explicabo.</li>
</ul>
</div>
<div class="col-12">
<p></p>
<blockquote>
At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi
sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.
It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a
more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
</blockquote>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>

<?php                   // Memulai session
include 'footer.php';                     // Panggil koneksi ke database
?>