<?php                   // Memulai session
include 'navbar.php';                     // Panggil koneksi ke database
?>
<br><br><br><br><br><br>
<div class="content">
<section class="bkg-light medium">
<div class="container">
<div class="row">
<div class="col-12">
<div class="boxed-container">
<div class="authentication">
<div class="block">
<div class="row">
<div class="col-12">
<div class="section-header text-center">
<h3>Tanjung Raja - Admin Login</h3>
</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-md-6">
<div class="form-group">
<form action="login-action.php" method="post">
<input type="text" class="form-control" name="username" value="" placeholder="Username">
</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-md-6">
<div class="form-group">
<input type="password" class="form-control" name="password" value="" placeholder="Password">
</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-md-6">

</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-md-6">
<div class="text-center">
<div class="g-recaptcha" data-sitekey="6LfgeWoUAAAAAIQVuIfYHBF2rKaxcFBhEQ1ayR_L"></div>
<input type="hidden" id="contactform-recaptcha2">
<div class="help-block"></div>
</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-md-6 col-lg-4">
<div class="text-center">
<button type="submit" name="submit" class="btn btn-primary full-width" value="Submit">Log in</button>
</form><p>New ? <a href="registrasi.php">Sign Up here.</a></p>
</div>
</div>
</div>
</div>
<div class="block">

</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>

<?php                   // Memulai session
include 'footer.php';                     // Panggil koneksi ke database
?>