<?php session_start();                   
include 'config.php';      
include 'fungsi/base_url.php';     
include 'fungsi/cek_session_public.php';  

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<meta name="keywords" content="Premium HTML5 Template" />
<meta name="description" content="ThePure - Responsive Bootstrap Classified Ads HTML Template" />
<meta name="author" content="CodinBit.com" />
<title> Tanjung Raja | Market</title>
<link rel="shortcut icon" href="<?php echo $base_url ?>assets/images/logo-title.png"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>assets/vendor/font-awesome/css/all.css">
<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>assets/vendor/flags2/flag-css.css">
<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>assets/vendor/slick/slick.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>assets/vendor/select2/dist/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>assets/vendor/fileinput/fileinput.css">
<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>assets/vendor/ekko-lightbox/ekko-lightbox.css" />

<link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>assets/css/style.css">
</head>
<body>
<div class="page-loader loading">
<div class="loader">
<i class="fas fa-spinner fa-spin" aria-hidden="true"></i>
</div>
</div>
<div class="overlay-nav"></div>

<header class="header floating">
<div class="top-bar">
<div class="container">
<div class="row justify-content-center align-self-center h-100">
<div class="col-6 col-md-5 my-auto">
<ul>
<li><strong></strong></li>
</ul>
<!-- <div class="btn-group location">
<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
United States
</button>
<div class="dropdown-menu">
<a href="#">Canada</a>
<a href="#">Mexico</a>
<a href="#">Japan</a>
<a href="#">Australia</a>
</div>
</div> -->
</div>
<div class="col-6 col-md-7 my-auto text-right">
<ul>
<li><i class="fa fa-envelope-o"></i> <a href="#">customer@website.com</a> </li>
<li><i class="fa fa-phone"></i> <a href="#">Tanjung Raja Lampung Utara kode Pos 34557</a> </li>
</ul>
<div class="btn-group language">
<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<span class="flag flag-idn"></span> IDN
</button>
<div class="dropdown-menu">

</div>
</div>
</div>
</div>
</div>
</div>
<div class="main-menu-wrapper">
<nav>
<div class="main-menu">
<div class="container">
<div class="row">
<div class="col-7 col-sm-7 col-lg-2">
<div class="hamburger"><a href="#"><span><span></span></span></a></div>
<div class="logo">
<a href="index.php"><img src="<?php echo $base_url ?>assets/images/logo-lampung.png"  alt="ThePure Template Logo" /></a>
</div>
<ul class="main-nav small-screen">
<li><a href="index.php">Home</a>
</li>
<li><a href="#">Category</a>
<ul>
            <?php
                $data     = mysqli_query($conn, "SELECT * FROM tb_kategori ");
                $numrows  = mysqli_num_rows($data);
                if($numrows > 0)
                    {
                         while($row = mysqli_fetch_assoc($data))
                        {   
            ?>
                    <li><a href="search-kategori.php?id_kategori=<?php echo $row['id_kategori']; ?>"><?php echo $row['nama_kategori']; ?></a></li>
            <?php
                        }
                    }
                   
            ?>
</ul>
</li>
<li><a href="#">About</a>

</li>
<li><a href="#">Contact</a>

</li>
<li><a href="contact.html">Sitemaps</a>

</li>
</ul>
</div>
<div class="col-12 col-sm-1 col-lg-8 d-none d-sm-block">
<ul class="main-nav">
<li ><a href="index.php">Home</a>

</li>
<li><a href="#">Category</a>
<ul>
            <?php
                $data     = mysqli_query($conn, "SELECT * FROM tb_kategori ");
                $numrows  = mysqli_num_rows($data);
                if($numrows > 0)
                    {
                         while($row = mysqli_fetch_assoc($data))
                        {   
            ?>
                    <li><a href="search-kategori.php?id_kategori=<?php echo $row['id_kategori']; ?>"><?php echo $row['nama_kategori']; ?></a>
            <?php
                $id_kategori = $row['id_kategori'];
                    $data2     = mysqli_query($conn, "SELECT * FROM tb_sub_kategori WHERE id_kategori = $id_kategori ");
                $numrows2  = mysqli_num_rows($data2);
                if($numrows2 > 0)
                    {
                        echo "<ul>";
                         while($row2 = mysqli_fetch_assoc($data2))
                        {
            ?> 
                        <li><a href="search-kategori.php?id_kategori=<?php echo $row['id_kategori']; ?>"><?php echo $row2['nama_sub_kategori']; ?></a></li>
                    <?php
                                }
                                echo "</ul>";
                            }
                            else{
                                echo "</li>";
                            }
                    ?>
            <?php
                        }
                    }
                    else{
                        echo "<script>alert('Data tidak ditemukan');location.replace('$base_url')</script>";
                    }
            ?>
</ul>
</li>
<li><a href="about.php">About</a>
</li>
<li><a href="contact.php">Contact</a>
</li>
<li><a href="sitemaps.php">SiteMaps</a>
</li>
</ul>
</div>


<?php
    if(!empty($_SESSION['username']) && empty($_SESSION['usertype']))
{ ?>
        <div class="col-5 col-sm-4 col-lg-2">
        <div class="right-block text-right">

        <a href="my-account-info.html" class="btn d-lg-none"><i class="far fa-user" aria-hidden="true"></i></a>
        <div class="btn-group d-none d-lg-block">

        <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="far fa-user" aria-hidden="true"></i>
        </a>
            <div class="dropdown-menu">
                <a href="my-post.php"><i class="fas fa-server" aria-hidden="true"></i>My Post</a>
                <a href="my-account.php"><i class="fas fa-cog" aria-hidden="true"></i>Account Info</a>
                
                <a href="logout.php"><i class="fas fa-sign-out-alt" aria-hidden="true"></i>Logout</a>
            </div>
        </div>
        <a href="post.php" class="btn btn-secondary"><i class="fa fa-plus" aria-hidden="true"></i><span>Post</span></a>
        </div>
       
   <?php  }else{ ?>
  
    <div class="col-5 col-sm-4 col-lg-2">
        <div class="right-block text-right">
            <a href="my-account-info.html" class="btn d-lg-none"><i class="far fa-user" aria-hidden="true"></i></a>
            <div class="btn-group d-none d-lg-block">

            <a href="login.php" class="btn dropdown-toggle">
            <i class="far fa-user" aria-hidden="true"></i> Login
            </a>
            </div>
        </div>
    </div>

    <?php } ?>
    </div>
</div>
</div>
</div>
</nav>
</div>
</header>




