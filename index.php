﻿<?php                   // Memulai session
include 'navbar.php';                     // Panggil koneksi ke database
?>


<div class="content">
<section class="home-slider">
<div class="slider-single control-nav">
<div class="slider-item">
<div class="item" style="background-image: url('<?php echo $base_url ?>assets/images/home/Cengkeh_1.jpg');">
<div class="content-wrapper">
<div class="info">
<div class="info-header text-center">
<h3 class="heading">3 Manfaat Cengkeh untuk Kesehatan</h3>
<p class="caption-header">1. Membunuh bakteri penyebab penyakit <br> 2. Meningkatkan kesehatan hati <br> 3. Menjaga kesehatan tulang </p>
</div>
</div>
</div>
</div>
</div>
<div class="slider-item">
<div class="item" style="background-image: url('<?php echo $base_url ?>assets/images/home/KOPI.jpg');">
<div class="content-wrapper">
<div class="info">
<div class="info-header text-center">
<h3 class="heading"> 3 Manfaat Kopi Hitam bagi Kesehatan </h3>
<p class="caption-header">1. Stimuli Fungsi Saraf dan Menambah Energi<br>2. Mencegah Parkinson dan Alzheimer <br>3. Meredakan Sakit Kepala </p>                       
</div>

</div>
</div>
</div>
</div>
<div class="slider-item">
<div class="item" style="background-image: url('<?php echo $base_url ?>assets/images/home/LADA.jpg');">
<div class="content-wrapper">
<div class="info">
<div class="info-header text-center">
<h3 class="heading">3 Manfaat Lada Hitam untuk Kesehatan </h3>
<p class="caption-header">1. Melancarkan sistem pernapasan <br>2. Meredakan sakit gigi <br>3. Menjaga gula darah</p>                       
</div>

</div>
</div>
</div>
</div>
<div class="slider-item">
<div class="item" style="background-image: url('<?php echo $base_url ?>assets/images/home/SINGKONG.jpg');">
<div class="content-wrapper">
<div class="info">
<div class="info-header text-center">
<h3 class="heading">2 Kandungan nutrisi dan manfaat singkong</h3>
<p class="caption-header">1. Karbohidrat, Setiap 100 gram singkong mengandung 38 gram karbohidrat <br>2. Serat,  Singkong mengandung serat pangan dalam jumlah yang cukup tinggi, sehingga dapat mencegah sembelit </p>                       
</div>

</div>
</div>
</div>
</div>
</div>


</section>
<div class="home-categories no-padding-top">
<div class="container">
<div class="row">
<div class="col-12">
<div class="categories_wrapper">
<div class="category-search">
<div class="row">
<div class="col-12 col-lg-10">
<div class="row">
<div class="col-12 col-md-6">
<form action="search-result.php" method="post">
<div class="form-group">
<i class="fa fa-search" aria-hidden="true"></i>
<input type="text" class="form-control" name="keyword" placeholder="Cari Hasil Perkebunan Disini Berdasarkan Alamat.." value="">
</div>
</div>

<div class="col-12 col-md-6">
<div class="form-group icon">
<i class="fa fa-bars" aria-hidden="true"></i>
<select name="id_kategori" required>
<option value="">Pilih Kategori</option>
<?php
                $data     = mysqli_query($conn, "SELECT * FROM tb_kategori ");
                $numrows  = mysqli_num_rows($data);
                if($numrows > 0)
                    {
                         while($row = mysqli_fetch_assoc($data))
                        {   
            ?>
                    <!-- awal -->
                    <option value="<?php echo $row['id_kategori']; ?>"><?php echo $row['nama_kategori']; ?></option>
                    <!-- akhir -->
                    
            <?php
                        }
                    }
                   
            ?>


</select>
</div>
</div>
</div>
</div>
<div class="col-12 col-lg-2">
<div class="form-group">
<button class="btn btn-primary" type="submit" name="cari">Search</button>
</div>
</form>
</div>
</div>
            </div>
<div class="category-list">
<div class="row">
            <?php
                $data     = mysqli_query($conn, "SELECT * FROM tb_kategori ");
                $numrows  = mysqli_num_rows($data);
                if($numrows > 0)
                    {
                         while($row = mysqli_fetch_assoc($data))
                        {   
            ?>
                    <!-- awal -->
                    <div class="col-4 col-md-4 col-lg-4">
                    <div class="item">
                    <a href="search-kategori.php?id_kategori=<?php echo $row['id_kategori']; ?>">
                    <span class="icon"><i class="fa fa-shopping-bag" aria-hidden="true"></i></span>
                    <h6><?php echo $row['nama_kategori']; ?></h6>
                    </a>
                   
                    </div>
                    </div>
                    <!-- akhir -->
                    
            <?php
                        }
                    }
                   
            ?>
                    


<div class="col-4 col-md-3 col-lg-3 d-md-none">
<div class="item empty">
<a href="category-list.html">
<span class="icon"></span>
<h6></h6>
</a>
<span class="counter">
</span>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
<section class="listing-products grid-4">
<div class="container">
<div class="row">
<div class="col-12">
<div class="section-header text-center">
<h3>Produk Terbaru</h3>
</div>
</div>
</div>
<div class="row row-list">
<!-- awal -->

<?php

// Memanggil data dari tabel produk diurutkan dengan id_produk secara DESC dan dibatasi sesuai $start dan $per_halaman
$data     = mysqli_query($conn, "SELECT * FROM tb_hasil_perkebunan a 
                    JOIN tb_kategori b ON a.id_kategori = b.id_kategori 
                    JOIN tb_petani c ON a.id_petani = c.id_petani 
                    JOIN tb_alamat d ON c.id_alamat = d.id_alamat WHERE a.status = 1 ORDER BY a.id_hasil_pertanian DESC LIMIT 8");
$numrows  = mysqli_num_rows($data);
?>

<?php
// Jika data ketemu, maka akan ditampilkan dengan While
if($numrows > 0)
{
  while($row = mysqli_fetch_assoc($data))
  {
    
?>

<div class="col-12 col-md-6 col-lg-3 item">
<div class="item-wrapper">
<div class="image-wrapper">
<div class="promoted">
<span>New</span>
</div>
<a href="view-detail.php?id=<?php echo $row['id_hasil_pertanian']; ?>" class="image">
    <?php if( $row['nama_foto'] == NULL){ ?>
        <img class="" src="gambar/no-image.png" data-src="#" alt=""> 
    <?php }else{ ?>
        <img class="" src="gambar/<?php echo $row['nama_foto']; ?>" data-src="#" alt="">
    <?php } ?>
</a>
<div class="quick-actions">
<a href="view-detail.php?id=<?php echo $row['id_hasil_pertanian']; ?>" data-toggle="modal" class="btn btn-primary btn-circle"><i class="fa fa-eye" aria-hidden="true"></i></a>
<a href="view-detail.php?id=<?php echo $row['id_hasil_pertanian']; ?>" class="btn btn-primary btn-circle add-to-fav"><i class="fa fa-heart" aria-hidden="true"></i></a>
</div>
</div>
<div class="info-wrapper">
<div class="info">
<div class="category"><?php echo $row['nama_kategori']; ?></div>
<a href="view-detail.php?id=<?php echo $row['id_hasil_pertanian']; ?>" class="name"><?php echo $row['nama_hasil_perkebunan']; ?></a>
<span class="price"><?php echo $row['deskripsi']; ?></span>
<span class="location"><i class="fa fa-map-marker-alt" aria-hidden="true"></i> <?php echo $row['alamat']; ?></span>
<span class="posted"><i class="far fa-clock" aria-hidden="true"></i>Posted <?php echo $row['tanggal_post']; ?></span>
</div>
</div>
</div>
</div>
<!-- akhir -->         
  
  <?php
  // Mengakhiri pengulangan while
  }
}

?>

</div>
</div>
</section>

</div>


 <?php                   // Memulai session
include 'footer.php';                     // Panggil koneksi ke database
?>

