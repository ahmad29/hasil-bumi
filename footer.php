<footer class="footer">
<form action="#" method="post" class="newsletter-banner">
<div class="container">
<div class="row justify-content-center align-self-center h-100">
<div class="col-12 col-xl-10">
<div class="row justify-content-center">
<div class="col-12 col-md-8 col-lg-6 my-auto">
<label>Sign up For <strong>Newsletter:</strong></label>
</div>
<div class="col-12 col-md-8 col-lg-6 my-auto">
<div class="input-group">
<input type="text" class="form-control" placeholder="">
<div class="input-group-append">
<button class="btn btn-secondary" type="button">Sign Up</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
<div class="block-navigation">

<div class="container">
<div class="row">
<div class="col-12 col-lg-4">
<div class="logo">
<a href="#"><img src="images/logo-light.png" width="" height="" alt="" /></a>
</div>
<p class="info">
Aliquam accumsan dolor enim, et tincidunt augue cursus in felis in lorem efficitur molestie id eget erat. Vivamus nisi tellus, sodales at auctor non, blandit vel augue. Etiam quis libero feugiat, consequat vitae, venenatis nisl.
</p>
<ul class="social">
<li><a href="#" target="_blank"><i class="fab fa-facebook-square" aria-hidden="true"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-twitter-square" aria-hidden="true"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-google-plus-square" aria-hidden="true"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-pinterest-square" aria-hidden="true"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
<li><a href="#" target="_blank"><i class="fab fa-vk" aria-hidden="true"></i></a></li>
</ul>
</div>
<div class="col-12 col-lg-8">
<div class="row">
<div class="col-12 col-md-4">
<div class="heading">Categories</div>
<ul>
<li><a href="category-list-2.html">Auto & Moto</a></li>
<li><a href="category-list.html">Electronics & Appliances</a></li>
<li><a href="category-list-2.html">Fashion</a></li>
<li><a href="category-list.html">Home And Garden</a></li>
<li><a href="category-list-2.html">Jobs</a></li>
<li><a href="category-list.html">Pets</a></li>
<li><a href="category-list-2.html">Real Estate</a></li>
</ul>
</div>
<div class="col-12 col-md-4">
<div class="heading">Help</div>
<ul>
<li><a href="simple-page.html">Conditions</a></li>
<li><a href="simple-page.html">Contact</a></li>
<li><a href="simple-page.html">Privacy Policy</a></li>
</ul>
</div>
<div class="col-12 col-md-4">
<div class="heading">Blog</div>
<ul>
<li><a href="blog-article.html">About Us</a></li>
<li><a href="blog-article.html">Company</a></li>
<li><a href="blog-article.html">Business</a></li>
<li><a href="blog-article.html">Affiliate</a></li>
<li><a href="blog-article.html">Intern Program</a></li>
<li><a href="blog-article.html">Jobs</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="block-copyright">
<div class="container">
<div class="row">
<div class="col-12">
2018 &copy; ThePure Template by <a href="https://www.codinbit.com/">CodinBit</a>
</div>
</div>
</div>
</div>
</footer>

<div id="quick-view" role="dialog" class="modal fade quick-view">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-body">
<a href="javascript:;" class="x-close" data-dismiss="modal"><i class="fas fa-times" aria-hidden="true"></i></a>
<div class="quick-view-wrapper">
<div class="image-wrapper">
<div class="slider-single animation-slide height-auto dir-nav">
<div class="slider-item">
<img src="images/products/1000X675_boots.jpg" alt="" />
</div>
<div class="slider-item">
<img src="images/products/1000X675_boots.jpg" alt="" />
</div>
</div>
</div>
<div class="content-wrapper">
<div class="category">Fashion</div>
<a href="view-listing.html" class="name">Timberland Boots</a>
<span class="location"><i class="fa fa-map-marker-alt" aria-hidden="true"></i> California, United States</span>
<span class="posted"><i class="far fa-clock" aria-hidden="true"></i> Posted 2 hours ago</span>
<span class="price">$419.99</span>
<div class="excerpt">
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Etiam quis libero feugiat, consequat eros vitae,
venenatis nisl. Duis a porttitor mauris, nec aliquam est.
</div>
<a href="#"><i class="far fa-heart" aria-hidden="true"></i> Add to favorites</a>
<div>
<a href="view-listing.html" class="btn btn-primary">View Ad</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<script src="<?php echo $base_url ?>assets/vendor/jquery/jquery.min.js"></script>

<script src="<?php echo $base_url ?>assets/vendor/popper.min.js"></script>
<script src="<?php echo $base_url ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $base_url ?>assets/vendor/slick/slick.js"></script>
<script src="<?php echo $base_url ?>assets/vendor/select2/dist/js/select2.min.js"></script>
<script src="<?php echo $base_url ?>assets/vendor/ekko-lightbox/ekko-lightbox.js"></script>
<script src="<?php echo $base_url ?>assets/js/main.js"></script>


<script src="<?php echo $base_url ?>assets/vendor/ckeditor/ckeditor.js"></script>
<script src="<?php echo $base_url ?>assets/vendor/fileinput/fileinput.js"></script>
</body>

</html>