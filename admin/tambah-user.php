<?php 
  include "layout/header-form.php";

if(isset($_POST['simpan']))
{   
    $nama_lengkap    = mysqli_real_escape_string($conn, $_POST['nama_lengkap']);
    $email    = mysqli_real_escape_string($conn, $_POST['email']);
    $username    = mysqli_real_escape_string($conn, $_POST['username']);
    $password    = mysqli_real_escape_string($conn, $_POST['password']);
    $alamat    = mysqli_real_escape_string($conn, $_POST['alamat']);
    $no_hp    = mysqli_real_escape_string($conn, $_POST['no_hp']);
    $level    = mysqli_real_escape_string($conn, $_POST['level']);
    $status    = mysqli_real_escape_string($conn, $_POST['status']);

    $sql = "INSERT INTO tb_user VALUES ('','$username','$password','$nama_lengkap','$alamat','$email',
            '$no_hp','$level','$status',now())";
    
    if(mysqli_query($conn, $sql)){
          echo "<script>location.replace('data-user.php?tambah=true')</script>";
    }else{
        echo "Error updating record: " . mysqli_error($conn);
    }
}
?>

 
<div class="wrapper row-offcanvas row-offcanvas-left">
<?php 
  include "sidebar.php";
?> 
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--section starts-->
        <h1>
            Form user
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">
                    <i class="fa fa-fw ti-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">Tambah user</a>
            </li>
            
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-fw ti-move"></i> Form Tambah user
                        </h3>
                        <span class="pull-right">
                            <i class="fa fa-fw ti-angle-up clickable"></i>
                            <i class="fa fa-fw ti-close removepanel clickable"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="#">
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Nama Lengkap</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" 
                                           placeholder="Nama user" name="nama_lengkap" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" 
                                           placeholder="email" name="email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" 
                                           placeholder="Username" name="username" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" 
                                           placeholder="Password" name="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Alamat</label>
                                <div class="col-sm-10">
                               <input type="text" class="form-control" 
                                           placeholder="Alamat" name="alamat" required>
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">No HP</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" pattern="\d*" minlength="11" maxlength="13" placeholder="No HP" name="no_hp" required>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Level</label>
                                <div class="col-sm-10">
                                <select class="form-control" name="level" required>
                                    <option value=''> Pilih Level</option>
                                    <option value='admin'> Admin</option>
                                    <option value='camat'> Camat</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                <select class="form-control" name="status" required>
                                    <option value=''> Pilih Status</option>
                                    <option value='1'> Aktif</option>
                                    <option value='0'> Non Aktif</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        
                                    </label>
                                    <div class="col-sm-10 col-md-10">
                                    <button type="submit" name="simpan" class="btn btn-success"> Save</button>
                                    </div>
                                </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!--main content ends-->
        <div class="background-overlay"></div>
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->
</div>


<?php 
  include "layout/footer-form.php";
?>