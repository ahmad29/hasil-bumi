<?php 
  include "layout/header-form.php";

    if(isset($_POST['update']))
    {   
        $id_user    = mysqli_real_escape_string($conn, $_POST['id_user']);
        $nama_lengkap    = mysqli_real_escape_string($conn, $_POST['nama_lengkap']);
        $email    = mysqli_real_escape_string($conn, $_POST['email']);
        $username    = mysqli_real_escape_string($conn, $_POST['username']);
        $password    = mysqli_real_escape_string($conn, $_POST['password']);
        $alamat    = mysqli_real_escape_string($conn, $_POST['alamat']);
        $no_hp    = mysqli_real_escape_string($conn, $_POST['no_hp']);
        $level    = mysqli_real_escape_string($conn, $_POST['level']);
        $status    = mysqli_real_escape_string($conn, $_POST['status']);
        
        $sql = "UPDATE tb_user SET nama_lengkap = '$nama_lengkap', email = '$email',
                username = '$username', password = '$password', alamat = '$alamat',
                no_hp = '$no_hp', level = '$level', status_aktif = '$status' WHERE id_user = $id_user";
        
        if(mysqli_query($conn, $sql)){
                echo "<script>location.replace('data-user.php?update=true')</script>";
        }else{
            echo "Error updating record: " . mysqli_error($conn);
        }
    }
  
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $sql = "SELECT * FROM tb_user WHERE id_user = $id";
        $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0){
                while ($data = mysqli_fetch_array($result)){
                    $id_user = $data['id_user'];
                    $nama_lengkap = $data['nama_lengkap'];
                    $email = $data['email'];
                    $username = $data['username'];
                    $password = $data['password'];
                    $alamat = $data['alamat'];
                    $no_hp = $data['no_hp'];
                    $level = $data['level'];
                    $status_aktif = $data['status_aktif']; 
                } 
            }
    }
?>

 
<div class="wrapper row-offcanvas row-offcanvas-left">
<?php 
  include "sidebar.php";
?> 
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--section starts-->
        <h1>
            Form user
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">
                    <i class="fa fa-fw ti-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">Edit user</a>
            </li>
            
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-fw ti-move"></i> Form Edit user
                        </h3>
                        <span class="pull-right">
                            <i class="fa fa-fw ti-angle-up clickable"></i>
                            <i class="fa fa-fw ti-close removepanel clickable"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="#">
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Nama Lengkap</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" 
                                           placeholder="Nama user" name="id_user" value="<?php echo $id_user; ?>">
                                    <input type="text" class="form-control" 
                                           placeholder="Nama user" name="nama_lengkap" value="<?php echo $nama_lengkap; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" 
                                           placeholder="email" name="email" value="<?php echo $email; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" 
                                           placeholder="Username" name="username" value="<?php echo $username; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" 
                                           placeholder="Password" name="password" value="<?php echo $password; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Alamat</label>
                                <div class="col-sm-10">
                                
                                   <input type="text" class="form-control" 
                                           placeholder="Username" name="alamat" value="<?php echo $alamat; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">No HP</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" 
                                           placeholder="No HP" name="no_hp" value="<?php echo $no_hp; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Level</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="level" required>
                                    <option value=''> Pilih Level</option>

                                         <option value="admin" 
                                            <?php if( $level == 'admin'){ echo "selected"; }else{} ?>>Admin</option>
                                             <option value="camat" 
                                            <?php if( $level == 'camat'){ echo "selected"; }else{} ?>>Camat</option>
                                    
                                
                                    
                                               
                                
                                </select>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                <select class="form-control" name="status" required>
                                    <option value=''> Pilih Status</option>
                                    <option value="0" 
                                            <?php if( $status_aktif == '0'){ echo "selected"; }else{} ?>>Non Aktif</option>
                                             <option value="1" 
                                            <?php if( $status_aktif == '1'){ echo "selected"; }else{} ?>>Aktif</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        
                                    </label>
                                    <div class="col-sm-10 col-md-10">
                                    <button type="submit" name="update" class="btn btn-success"> Update</button>
                                    </div>
                                </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!--main content ends-->
        <div class="background-overlay"></div>
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->
</div>


<?php 

  include "layout/footer-form.php";
?>