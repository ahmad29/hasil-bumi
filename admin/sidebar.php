<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar-->
    <section class="sidebar">
        <div id="menu" role="navigation">
            <div class="nav_profile">
                <div class="media profile-left">
                    <a class="pull-left profile-thumb" href="#">
                        <img src="<?php echo $base_url ?>template/img/original.jpg" class="img-circle" alt="User Image"></a>
                    <div class="content-profile">
                        <h4 class="media-heading"><?php echo $_SESSION['username']; ?></h4>
                        <ul class="icon-list">
                            <li>
                                <a href="#">
                                    <i class="fa fa-fw ti-user"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-fw ti-lock"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-fw ti-settings"></i>
                                </a>
                            </li>
                            <li>
                                <a href="logout.php">
                                    <i class="fa fa-fw ti-shift-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="navigation">
                <li>
                    <a href="index.php">
                        <i class="menu-icon ti-home"></i>
                        <span class="mm-text ">Dashboard</span>
                    </a>
                </li>

                <?php if ($_SESSION['level'] == 'admin') { ?>
                    <li>
                        <a href="data-petani.php">
                            <i class="menu-icon ti-github"></i>
                            <span class="mm-text ">Data petani</span>
                        </a>
                    </li>
                    <li>
                        <a href="data-hasil-perkebunan.php">
                            <i class="menu-icon ti-clipboard"></i>
                            <span class="mm-text ">Data Hasil Perkebunan</span>
                        </a>
                    </li>
                    <li>
                        <a href="data-kategori.php">
                            <i class="menu-icon ti-home"></i>
                            <span class="mm-text "> Data Kategori</span>
                        </a>
                    </li>
                    <li>
                        <a href="approve-perkebunan.php">
                            <i class="menu-icon ti-clipboard"></i>
                            <span class="mm-text ">Approve Hasil Perkebunan</span>
                        </a>
                    </li>
                    <li>
                        <a href="data-alamat.php">
                            <i class="menu-icon ti-home"></i>
                            <span class="mm-text "> Data Alamat</span>
                        </a>
                    </li>
                    <li class="menu-dropdown">
                        <a href="#">
                            <i class="menu-icon ti-briefcase"></i>
                            <span>
                                Laporan
                            </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="lap-hasil-perkebunan.php">
                                    <i class="fa fa-fw ti-brush"></i> Hasil Perkebunan
                                </a>
                            </li>
                            <li>
                                <a href="lap-data-petani.php">
                                    <i class="fa fa-fw ti-layout-grid2"></i> Data Petani
                                </a>
                            </li>
                            <!-- <li>
                                <a href="lap-grafik.php">
                                    <i class="fa fa-fw ti-tag"></i> Grafik Hasil Perkebunan
                                </a>
                            </li> -->

                        </ul>
                    </li>
                <?php } ?>
                <?php if ($_SESSION['level'] == 'camat') { ?>
                    <li>
                        <a href="data-user.php">
                            <i class="menu-icon ti-user"></i>
                            <span class="mm-text">Data User</span>
                        </a>
                    </li>
                    <li class="menu-dropdown">
                        <a href="#">
                            <i class="menu-icon ti-briefcase"></i>
                            <span>
                                Laporan
                            </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="lap-hasil-perkebunan.php">
                                    <i class="fa fa-fw ti-brush"></i> Hasil Perkebunan
                                </a>
                            </li>
                            <li>
                                <a href="lap-data-petani.php">
                                    <i class="fa fa-fw ti-layout-grid2"></i> Data Petani
                                </a>
                            </li>
                            <!-- <li>
                                <a href="lap-grafik.php">
                                    <i class="fa fa-fw ti-tag"></i> Grafik Hasil Perkebunan
                                </a>
                            </li> -->

                        </ul>
                    </li>
                <?php } ?>
            </ul>
            <!-- / .navigation -->
        </div>
        <!-- menu -->
    </section>
    <!-- /.sidebar -->
</aside>