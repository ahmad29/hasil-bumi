<?php 
  include "layout/header-form.php";

if(isset($_POST['simpan']))
{   
    $kategori    = mysqli_real_escape_string($conn, $_POST['kategori']);
    $nama_hasil_perkebunan = mysqli_real_escape_string($conn, $_POST['nama_hasil_perkebunan']);
    $deskripsi    = mysqli_real_escape_string($conn, $_POST['deskripsi']);
    $luas_lahan    = mysqli_real_escape_string($conn, $_POST['luas_lahan']);
    $hasil_perlahan    = mysqli_real_escape_string($conn, $_POST['hasil_perlahan']);
    $panen_pertahun    = mysqli_real_escape_string($conn, $_POST['panen_pertahun']);
    $pendapatan_lahan    = mysqli_real_escape_string($conn, $_POST['pendapatan_lahan']);
    $waktu_musim    = mysqli_real_escape_string($conn, $_POST['waktu_musim']);

      $file_name    = $_FILES['img']['name']; // File adalah name dari tombol input pada form
      $file_tmp     = $_FILES['img']['tmp_name'];
      $lokasi       = 'gambar/'.$file_name.'';
      $gambar = $file_name;

      move_uploaded_file($file_tmp, $lokasi);
    $sql = "INSERT INTO tb_hasil_perkebunan VALUES ('','$kategori','$nama_hasil_perkebunan',
        '$deskripsi','$luas_lahan','$hasil_perlahan','$panen_permusim','$pendapatan_lahan',
        '$waktu_musim','$id_petani','$gambar','0', now(), '$username')";
    
    if(mysqli_query($conn, $sql)){
          echo "<script>location.replace('data-hasil-perkebunan.php?tambah=true')</script>";
    }else{
        echo "Error updating record: " . mysqli_error($conn);
    }
}
?>

 
<div class="wrapper row-offcanvas row-offcanvas-left">
<?php 
  include "sidebar.php";
?> 
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--section starts-->
        <h1>
            Form Hasil Pertanian
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">
                    <i class="fa fa-fw ti-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">Tambah Hasil Pertanian</a>
            </li>
            
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-fw ti-move"></i> Form Tambah Hasil Pertanian
                        </h3>
                        <span class="pull-right">
                            <i class="fa fa-fw ti-angle-up clickable"></i>
                            <i class="fa fa-fw ti-close removepanel clickable"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="#">
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Kategori</label>
                                <div class="col-sm-10">
                                <select class="form-control" name="kategori" required>
                                    <option value=''> Pilih Kategori</option>
                                <?php
                                    $data     = mysqli_query($conn, "SELECT * FROM tb_kategori ");
                                    $numrows  = mysqli_num_rows($data);
                                    if($numrows > 0){
                                        while($row = mysqli_fetch_assoc($data)){   
                                    ?>
                                         <option value="<?php echo $row['id_kategori']; ?>"><?php echo $row['nama_kategori']; ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                
                                    
                                               
                                
                                </select>
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Nama Hasil Perkebunan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" 
                                           placeholder="Contoh: kopi robinson, dll" name="nama_hasil_pertanian" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Deskripsi</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" 
                                           placeholder="Deskripsi" name="deskripsi" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Luas Lahan</label>
                                <div class="col-sm-10">
                                   
        <select name="luas_lahan" class="form-control"  required>
          <option value="">Pilih Luas Lahan</option>
           <?php
          for ($x = 1; $x <= 100; $x++) {
            ?>
             <option value="<?php echo $x; ?>"><?php echo $x; ?> Hektar</option>";
              <?php
          }
          ?> 


        </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Hasil Per Lahan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" 
                                           placeholder="Contoh: 10 KG/KW/TON" name="hasil_perlahan" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Panen Pertahun</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" 
                                           placeholder="Contoh: 1 kali pertahun" name="panen_pertahun" required>
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Pendapatan Lahan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" 
                                           placeholder="Contoh: Rp 2.000.000" name="pendapatan_lahan" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label"> Waktu Musim</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" 
                                           placeholder="Contoh: JUNI-JULI" name="waktu_musim" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Petani</label>
                                <div class="col-sm-10">
                                <select class="form-control" name="kategori" required>
                                    <option value=''> Pilih Petani</option>
                                <?php
                                    $data     = mysqli_query($conn, "SELECT * FROM tb_petani ");
                                    $numrows  = mysqli_num_rows($data);
                                    if($numrows > 0){
                                        while($row = mysqli_fetch_assoc($data)){   
                                    ?>
                                         <option value="<?php echo $row['id_petani']; ?>"><?php echo $row['nama_petani']; ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                
                                    
                                               
                                
                                </select>
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Foto</label>
                                <div class="col-sm-10">
                                <input type="file" name="img" id="img" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        
                                    </label>
                                    <div class="col-sm-10 col-md-10">
                                    <button type="submit" name="simpan" class="btn btn-success"> Save</button>
                                    </div>
                                </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!--main content ends-->
        <div class="background-overlay"></div>
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->
</div>


<?php 
  include "layout/footer-form.php";
?>