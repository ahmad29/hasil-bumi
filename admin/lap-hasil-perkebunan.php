<?php 
  include "layout/header-form.php";

if(isset($_POST['simpan']))
{   
    $alamat    = mysqli_real_escape_string($conn, $_POST['alamat']);
    $sql = "INSERT INTO tb_alamat (alamat) VALUES ('$alamat')";
    
    if(mysqli_query($conn, $sql)){
          echo "<script>location.replace('data-alamat.php?tambah=true')</script>";
    }else{
        echo "Error updating record: " . mysqli_error($conn);
    }
}
?>

 
<div class="wrapper row-offcanvas row-offcanvas-left">
<?php 
  include "sidebar.php";
?> 
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--section starts-->
        <h1>
            Laporan Hasil Perkebunan
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">
                    <i class="fa fa-fw ti-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">Laporan Hasil Perkebunan</a>
            </li>
            
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <br>
                    <div class="panel-body">
                    <br>
                        <form class="form-horizontal" role="form" method="POST" action="#">
                            <div class="form-group">
                                <label for="input-text" class="col-sm-1">Petani</label>
                                <div class="col-sm-2">
                                <select class="form-control" name="id_petani" required>
                                    <option value=''> Pilih Petani</option>
                                    
                                <?php
                                    $data     = mysqli_query($conn, "SELECT * FROM tb_petani ");
                                    $numrows  = mysqli_num_rows($data);
                                    if($numrows > 0){
                                        while($row = mysqli_fetch_assoc($data)){   
                                    ?>
                                         <option value="<?php echo $row['id_petani']; ?>"><?php echo $row['nama_petani']; ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                
                                <!-- <option value="all">Semua Petani</option> -->
                                               
                                
                                </select>
                                </div>
                                <label for="input-text" class="col-sm-1">Tahun</label>
                                <div class="col-sm-2">
                                <input class="form-control" name="tahun" type="number" value="2019" min="2010" max='<?php echo date("Y"); ?>'/>
                                    <!-- <input type="text" class="form-control" 
                                           placeholder="alamat" name="alamat"> -->
                                </div>
                                <label for="input-text" class="col-sm-1">Kategori</label>
                                <div class="col-sm-2">
                                <select class="form-control" name="id_kategori" required>
                                    <option value=''> Pilih kategori</option>
                                    
                                <?php
                                    $data     = mysqli_query($conn, "SELECT * FROM tb_kategori ");
                                    $numrows  = mysqli_num_rows($data);
                                    if($numrows > 0){
                                        while($row = mysqli_fetch_assoc($data)){   
                                    ?>
                                         <option value="<?php echo $row['id_kategori']; ?>"><?php echo $row['nama_kategori']; ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                
                                <!-- <option value="all">Semua kategori</option> -->
                                </select>
                                </div>
                                <div class="col-sm-2">
                                <button type="submit" name="proses" class="btn btn-success"> Proses</button>
                                </div>
                            </div>
                            
                            
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php  
						if (isset($_POST['proses'])) {
              $id_petani = $_POST['id_petani'];
              $tahun  = $_POST['tahun'];
              $id_kategori  = $_POST['id_kategori'];
              
          $query1        = "SELECT *
                            FROM tb_hasil_perkebunan a JOIN tb_petani b ON a.id_petani = b.id_petani 
                            JOIN tb_alamat d ON b.id_alamat = d.id_alamat
                            JOIN tb_kategori c ON a.id_kategori = c.id_kategori 
                            WHERE a.id_kategori = '$id_kategori' AND a.id_petani = '$id_petani' AND YEAR(a.tanggal_post) = '$tahun'";
          $hasil1        = mysqli_query($conn,$query1);

          
          if(mysqli_num_rows($hasil1) == 0)
          {echo "<center><h4>Tidak Ada Hasil</h4></center>";}
            else
							{
					
            echo "
            
            <div class='box'>
        
          <div class='box-body table-responsive padding'>
            
            <div class='panel-heading' align='center'>
            <a href='print-hasil-perkebunan.php?id_petani=$id_petani&&tahun=$tahun&&id_kategori=$id_kategori'  class='btn btn-success' target='_BLANK'><i class='fa fa-print'></i> Cetak</a>
            </div>
            <table class='col-md-12 table-bordered table-striped table-condensed cf'>
      <thead class='cf'>
        <tr>
          <td align='center'>No.</td>
          <td align='center'>Petani</td>
          <td align='center'>Alamat</td>
          <td align='center'>Nama Hasil Perkebunan</td>
          <td align='center'>Kategori</td>
          <td align='center'>Tanggal</td>
          <td align='center'>Hasil</td>
          
          
        </tr>
      </thead>";

    $no = 1;
    $total = 0;
    while($data = mysqli_fetch_array($hasil1))
    {
      $hasil = $data['luas_lahan'] * $data['hasil_per_lahan'];
     

      echo "
      <tbody>
        <tr>
          <td data-title='No.' align='center'>".$no."</td>
          </td><td data-title='Harga Diskon' align='center'>$data[nama_petani]</td>
          </td><td data-title='Harga Diskon' align='center'>$data[alamat]</td>
          </td><td data-title='Harga Diskon' align='center'>$data[nama_hasil_perkebunan]</td>
          </td><td data-title='Harga Diskon' align='center'>$data[nama_kategori]</td>
          
          </td><td data-title='Harga Diskon' align='center'>$data[tanggal_post]</td>
          </td><td data-title='Harga Diskon' align='center'>$hasil TON</td>
          
        </tr>";
      $no++;
      $total += $hasil;
    }
    echo "
      
    <tr>
      <td data-title='No.' colspan='6' align='right'><b> TOTAL</b></td>
      
      <td data-title='Harga Diskon' align='center'><b>$total TON</b></td>
      
      
    </tr>
    </tbody>
          </table>";
    
		?>
					<?php
             } }?>
    

        <!--main content ends-->
        <div class="background-overlay"></div>
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->
</div>


<?php 
  include "layout/footer-form.php";
?>