<?php 
  include "layout/header-form.php";

    if(isset($_POST['update']))
    {   
        $id_alamat    = mysqli_real_escape_string($conn, $_POST['id_alamat']);
        $alamat    = mysqli_real_escape_string($conn, $_POST['alamat']);
        
        $sql = "UPDATE tb_alamat SET alamat = '$alamat' WHERE id_alamat = $id_alamat";
        
        if(mysqli_query($conn, $sql)){
                echo "<script>location.replace('data-alamat.php?update=true')</script>";
        }else{
            echo "Error updating record: " . mysqli_error($conn);
        }
    }
  
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $sql = "SELECT * FROM tb_alamat WHERE id_alamat = $id";
        $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0){
                while ($data = mysqli_fetch_array($result)){
                    $id_alamat = $data['id_alamat'];
                    $alamat = $data['alamat']; 
                } 
            }
    }
?>

 
<div class="wrapper row-offcanvas row-offcanvas-left">
<?php 
  include "sidebar.php";
?> 
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--section starts-->
        <h1>
            Form alamat
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">
                    <i class="fa fa-fw ti-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">Tambah alamat</a>
            </li>
            
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-fw ti-move"></i> Form Tambah alamat
                        </h3>
                        <span class="pull-right">
                            <i class="fa fa-fw ti-angle-up clickable"></i>
                            <i class="fa fa-fw ti-close removepanel clickable"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="#">
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">alamat</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" 
                                           placeholder="alamat" name="id_alamat" value="<?php echo $id_alamat; ?>">
                                    <input type="text" class="form-control" 
                                           placeholder="alamat" name="alamat" value="<?php echo $alamat; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        
                                    </label>
                                    <div class="col-sm-10 col-md-10">
                                    <button type="submit" name="update" class="btn btn-success"> Update</button>
                                    </div>
                                </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!--main content ends-->
        <div class="background-overlay"></div>
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->
</div>


<?php 

  include "layout/footer-form.php";
?>