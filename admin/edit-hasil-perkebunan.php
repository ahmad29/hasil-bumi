<?php 
  include "layout/header-form.php";

    if(isset($_POST['update']))
    {   
        $id_hasil_pertanian    = mysqli_real_escape_string($conn, $_POST['id_hasil_pertanian']);
        $status    = mysqli_real_escape_string($conn, $_POST['status']);
        
        $sql = "UPDATE tb_hasil_pertanian SET status = '$status' WHERE id_hasil_pertanian = $id_hasil_pertanian";
        
        if(mysqli_query($conn, $sql)){
                echo "<script>location.replace('approve-perkebunan.php?update=true')</script>";
        }else{
            echo "Error updating record: " . mysqli_error($conn);
        }
    }
  
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $sql = "SELECT * FROM tb_hasil_pertanian WHERE id_hasil_pertanian = $id";
        $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0){
                while ($data = mysqli_fetch_array($result)){
                    $id_hasil_pertanian = $data['id_hasil_pertanian'];
                    $status = $data['status'];
                    
                } 
            }
    }
?>

 
<div class="wrapper row-offcanvas row-offcanvas-left">
<?php 
  include "sidebar.php";
?> 
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--section starts-->
        <h1>
            Form Approve
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">
                    <i class="fa fa-fw ti-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">Approve Hasil Perkebunan</a>
            </li>
            
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-fw ti-move"></i> Form Approve Hasil Perkebunan
                        </h3>
                        <span class="pull-right">
                            <i class="fa fa-fw ti-angle-up clickable"></i>
                            <i class="fa fa-fw ti-close removepanel clickable"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="#">
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" 
                                           placeholder="alamat" name="id_hasil_pertanian" value="<?php echo $id_hasil_pertanian; ?>">
                                    
                                    <select class="form-control" name="status" required>

                                    <?php
                                    
                                    if ($status== 0 ) echo "<option value='0' selected> Non Aktif</option>";
                                    else echo "<option value='0'> Non Aktif</option>";
                                    if ($status== 1 ) echo "<option value='1' selected> Aktif</option>";
                                    else echo "<option value='1'> Aktif</option>";                     
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        
                                    </label>
                                    <div class="col-sm-10 col-md-10">
                                    <button type="submit" name="update" class="btn btn-success"> Update</button>
                                    </div>
                                </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!--main content ends-->
        <div class="background-overlay"></div>
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->
</div>


<?php 

  include "layout/footer-form.php";
?>