<?php session_start();
include '../config.php';
include '../fungsi/base_url.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Form Elements | Clear Admin Template</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="<?php echo $base_url ?>template/img/favicon.ico" />
    <link type="text/css" href="<?php echo $base_url ?>template/css/app.css" rel="stylesheet" />
    <link href="<?php echo $base_url ?>template/vendors/iCheck/css/all.css" rel="stylesheet" />
    <link href="<?php echo $base_url ?>template/vendors/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>template/css/custom.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>template/css/custom_css/skins/skin-default.css" type="text/css" id="skin" />
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>template/css/formelements.css">

</head>

<body class="skin-default">
    <div class="preloader">
        <div class="loader_img"><img src="<?php echo $base_url ?>template/img/loader.gif" alt="loading..." height="64" width="64"></div>
    </div>
    <header class="header">
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the marginin -->
                <img src="<?php echo $base_url ?>template/img/logo-2-putih.png" width="150" alt="logo" />
            </a>
            <!-- Header Navbar: style can be found in header-->
            <!-- Sidebar toggle button-->
            <div>
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i class="fa fa-fw ti-menu"></i>
                </a>
            </div>
            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <?php
                    $sql = "SELECT COUNT(id_hasil_pertanian) as jumlah FROM tb_hasil_perkebunan 
                WHERE status = '0' ";
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        while ($data = mysqli_fetch_array($result)) {

                            if ($data['jumlah'] == 0) { ?>

                                <li class="dropdown messages-menu">
                                    <a href="data-hasil-perkebunan.php"> <i class="fa fa-fw ti-email black"></i>

                                    </a>

                                </li>


                            <?php } else { ?>




                                <li class="dropdown messages-menu">
                                    <a href="data-hasil-perkebunan.php"> <i class="fa fa-fw ti-email black"></i>
                                        <span class="label label-success"><?php echo $data['jumlah']; ?></span>
                                    </a>

                                </li>
                    <?php  }
                        }
                    }
                    ?>


                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle padding-user" data-toggle="dropdown">
                            <img src="<?php echo $base_url ?>template/img/original.jpg" width="35" class="img-circle img-responsive pull-left" height="35" alt="User Image">
                            <div class="riot">
                                <div>
                                    <?php echo $_SESSION['username']; ?>
                                    <span>
                                        <i class="caret"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo $base_url ?>template/img/original.jpg" class="img-circle" alt="User Image">
                                <p> <?php echo $_SESSION['username']; ?></p>
                            </li>
                            <!-- Menu Body -->

                            <li role="presentation" class="divider"></li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">

                                </div>
                                <div class="pull-right">
                                    <a href="logout.php">
                                        <i class="fa fa-fw ti-shift-right"></i>
                                        Logout
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>