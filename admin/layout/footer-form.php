
<script src="<?php echo $base_url ?>template/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/g/bootstrap@3.3.7,bootstrap.switch@3.3.2,jquery.nicescroll@3.6.0"></script>
<script src="<?php echo $base_url ?>template/js/app.js" type="text/javascript"></script><!-- end of global js -->

<script src="<?php echo $base_url ?>template/vendors/iCheck/js/icheck.js"></script>
<script src="<?php echo $base_url ?>template/vendors/bootstrap-fileinput/js/fileinput.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url ?>template/js/custom_js/form_elements.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
</body>

</html>