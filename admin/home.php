<?php session_start();
include '../config.php';
include '../fungsi/base_url.php';
?>
<!DOCTYPE html>
<html>

<!-- Mirrored from demo.vueadmintemplate.com/dark/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Apr 2019 07:12:26 GMT -->

<head>
    <meta charset="UTF-8">
    <title>Tanjung Raja | Market</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="<?php echo $base_url ?>template/img/favicon.ico" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link type="text/css" rel="stylesheet" href="<?php echo $base_url ?>template/css/app.css" />
    <!-- end of global css -->
    <!--page level css -->
    <link rel="stylesheet" href="<?php echo $base_url ?>template/vendors/swiper/css/swiper.min.css">
    <link href="<?php echo $base_url ?>template/vendors/nvd3/css/nv.d3.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo $base_url ?>template/vendors/lcswitch/css/lc_switch.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>template/css/custom.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>template/css/custom_css/skins/skin-default.css" type="text/css" id="skin" />
    <link href="<?php echo $base_url ?>template/css/custom_css/dashboard1.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $base_url ?>template/css/custom_css/dashboard1_timeline.css" rel="stylesheet" />

    <!--end of page level css-->
</head>

<body class="skin-default">
    <div class="preloader">
        <div class="loader_img"><img src="<?php echo $base_url ?>template/img/loader.gif" alt="loading..." height="64" width="64"></div>
    </div>
    <!-- header logo: style can be found in header-->
    <header class="header">
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the marginin -->
                <img src="<?php echo $base_url ?>template/img/logo-2-putih.png" width="150" alt="logo" />
            </a>
            <!-- Header Navbar: style can be found in header-->
            <!-- Sidebar toggle button-->
            <div>
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i class="fa fa-fw ti-menu"></i>
                </a>
            </div>
            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <?php
                    $sql = "SELECT COUNT(id_hasil_pertanian) as jumlah FROM tb_hasil_perkebunan 
                WHERE status = '0' ";
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        while ($data = mysqli_fetch_array($result)) {

                            if ($data['jumlah'] == 0) { ?>

                                <li class="dropdown messages-menu">
                                    <a href="data-hasil-perkebunan.php"> <i class="fa fa-fw ti-email black"></i>

                                    </a>

                                </li>


                            <?php } else { ?>




                                <li class="dropdown messages-menu">
                                    <a href="data-hasil-perkebunan.php"> <i class="fa fa-fw ti-email black"></i>
                                        <span class="label label-success"><?php echo $data['jumlah']; ?></span>
                                    </a>

                                </li>
                    <?php  }
                        }
                    }
                    ?>


                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle padding-user" data-toggle="dropdown">
                            <img src="<?php echo $base_url ?>template/img/original.jpg" width="35" class="img-circle img-responsive pull-left" height="35" alt="User Image">
                            <div class="riot">
                                <div>
                                    <?php echo $_SESSION['username']; ?>
                                    <span>
                                        <i class="caret"></i>
                                    </span>
                                </div>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo $base_url ?>template/img/original.jpg" class="img-circle" alt="User Image">
                                <p> <?php echo $_SESSION['username']; ?></p>
                            </li>
                            <!-- Menu Body -->

                            <li role="presentation" class="divider"></li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">

                                </div>
                                <div class="pull-right">
                                    <a href="logout.php">
                                        <i class="fa fa-fw ti-shift-right"></i>
                                        Logout
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="wrapper row-offcanvas row-offcanvas-left">

        <?php
        include "sidebar.php";
        ?>
        <aside class="right-side">

            <section class="content-header">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-5 col-xs-8">
                        <div class="header-element">
                            <h3>Admin /
                                <small>Dashboard</small>
                            </h3>
                        </div>
                    </div>
                    <div class="col-lg-4 col-lg-offset-2 col-md-6 col-sm-7 col-xs-4">
                        <div class="header-object">
                            <span class="option-search pull-right hidden-xs">
                                <span class="search-wrapper">
                                    <input type="text" placeholder="Search here"><i class="ti-search"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="flip">
                            <div class="widget-bg-color-icon card-box front">
                                <div class="bg-icon pull-left">
                                    <i class="ti-eye text-warning"></i>
                                </div>
                                <div class="text-right">
                                    <?php
                                    $sql = "SELECT COUNT(id_hasil_pertanian) as jumlah FROM tb_hasil_perkebunan 
                WHERE status = '1' ";
                                    $result = mysqli_query($conn, $sql);

                                    if (mysqli_num_rows($result) > 0) {
                                        while ($data = mysqli_fetch_array($result)) {
                                            ?>
                                            <h3 class="text-dark"><b><?php echo $data['jumlah']; ?></b></h3>
                                    <?php
                                        }
                                    }
                                    ?>
                                    <p>Hasil Perkebunan</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="widget-bg-color-icon card-box back">
                                <div class="text-center">
                                    <span id="loadspark-chart"></span>
                                    <hr>
                                    <p>Check summary</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="flip">
                            <div class="widget-bg-color-icon card-box front">
                                <div class="bg-icon pull-left">
                                    <i class="ti-shopping-cart text-success"></i>
                                </div>
                                <div class="text-right">
                                    <?php
                                    $sql = "SELECT COUNT(id_hasil_pertanian) as jumlah FROM tb_hasil_perkebunan 
                WHERE status = '0' ";
                                    $result = mysqli_query($conn, $sql);

                                    if (mysqli_num_rows($result) > 0) {
                                        while ($data = mysqli_fetch_array($result)) {
                                            ?>
                                            <h3 class="text-dark"><b><?php echo $data['jumlah']; ?></b></h3>
                                    <?php
                                        }
                                    }
                                    ?>
                                    <p>Post Baru</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="widget-bg-color-icon card-box back">
                                <div class="text-center">
                                    <span class="linechart" id="salesspark-chart"></span>
                                    <hr>
                                    <p>Check summary</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="flip">
                            <div class="widget-bg-color-icon card-box front">
                                <div class="bg-icon pull-left">
                                    <i class="ti-user text-warning"></i>
                                </div>
                                <div class="text-right">
                                    <?php
                                    $sql = "SELECT COUNT(id_petani) as jumlah FROM tb_petani
                ";
                                    $result = mysqli_query($conn, $sql);

                                    if (mysqli_num_rows($result) > 0) {
                                        while ($data = mysqli_fetch_array($result)) {
                                            ?>
                                            <h3 class="text-dark"><b><?php echo $data['jumlah']; ?></b></h3>
                                    <?php
                                        }
                                    }
                                    ?>
                                    <p>Petani</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="widget-bg-color-icon card-box back">
                                <div class="text-center">
                                    <span id="visitsspark-chart"></span>
                                    <hr>
                                    <p>Check summary</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="background-overlay"></div>
            </section>
            <!-- /.content -->
        </aside>
        <!-- /.right-side -->
    </div>
    <!-- ./wrapper -->
    <!-- global js -->
    <div id="qn"></div>
    <script src="<?php echo $base_url ?>template/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/g/bootstrap@3.3.7,bootstrap.switch@3.3.2,jquery.nicescroll@3.6.0"></script>
    <script src="<?php echo $base_url ?>template/js/app.js" type="text/javascript"></script><!-- end of global js -->

    <!-- begining of page level js -->
    <!--Sparkline Chart-->
    <script type="text/javascript" src="<?php echo $base_url ?>template/js/custom_js/sparkline/jquery.flot.spline.js"></script>

    <script type="text/javascript" src="<?php echo $base_url ?>template/vendors/flip/js/jquery.flip.min.js"></script>
    <script type="text/javascript" src="<?php echo $base_url ?>template/vendors/lcswitch/js/lc_switch.min.js"></script>

    <script type="text/javascript" src="<?php echo $base_url ?>template/vendors/flotchart/js/jquery.flot.js"></script>
    <script type="text/javascript" src="<?php echo $base_url ?>template/vendors/flotchart/js/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="<?php echo $base_url ?>template/vendors/flotchart/js/jquery.flot.stack.js"></script>
    <script type="text/javascript" src="<?php echo $base_url ?>template/vendors/flotspline/js/jquery.flot.spline.min.js"></script>
    <script type="text/javascript" src="<?php echo $base_url ?>template/vendors/flot.tooltip/js/jquery.flot.tooltip.js"></script>
    <!--swiper-->
    <script type="text/javascript" src="<?php echo $base_url ?>template/vendors/swiper/js/swiper.min.js"></script>
    <!--chartjs-->
    <script src="<?php echo $base_url ?>template/vendors/chartjs/js/Chart.js"></script>
    <!--nvd3 chart-->
    <script type="text/javascript" src="<?php echo $base_url ?>template/js/nvd3/d3.v3.min.js"></script>
    <script type="text/javascript" src="<?php echo $base_url ?>template/vendors/nvd3/js/nv.d3.min.js"></script>
    <!-- <script type="text/javascript" src="vendors/moment/js/moment.min.js"></script> -->
    <script type="text/javascript" src="<?php echo $base_url ?>template/vendors/advanced_newsTicker/js/newsTicker.js"></script>
    <script type="text/javascript" src="<?php echo $base_url ?>template/js/dashboard1.js"></script>
    <!-- end of page level js -->

</body>


<!-- Mirrored from demo.vueadmintemplate.com/dark/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Apr 2019 07:14:11 GMT -->

</html>