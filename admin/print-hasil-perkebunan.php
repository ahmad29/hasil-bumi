<?php session_start(); ob_start();
include '../config.php';  
include '../fungsi/cek_session.php';      // Panggil fungsi cek session                   // Panggil koneksi ke database
$dari = $_GET['tahun'];


?>

<html xmlns="http://www.w3.org/1999/xhtml"> <!-- Bagian halaman HTML yang akan konvert -->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title> Laporan Data Pengunjung</title>
    <style type="text/css">
		.tabel2 {
		  width: 100%;
		  border-collapse: collapse;
		  border-spacing: 0;
		}
		.tabel2 tr.odd td {
		    background-color: #f9f9f9;
		}
		.tabel2 th, .tabel2 td {
	    padding: 4px 5px;
	    line-height: 20px;
	    text-align: left;
	    vertical-align: top;
	    border: 1px solid;
		}
		</style>
  </head>
  <body>
				<table style="width: 100%;">
            <tr>
                <td style="text-align: center;    width: 15%"></td>
								<td style="text-align: center;    width: 75%"><font style="font-size: 15px; text-align: center"><b>PEMERINTAH KABUPATEN LAMPUNG UTARA</b>
									</font><br/><font style="font-size: 10px; text-align: center"><b>Kecamatan Tanjung Raja<br/> Kode Pos. 34557</b></font></td>
                
            </tr>
				</table>
				<hr/>
				<br/>
				<table style="width: 100%;">
            <tr>
                
								<td style="text-align: center;    width: 100%"><font style="font-size: 15px; text-align: center">LAPORAN DATA HASIL PERKEBUNAN</font></td>
                
            </tr>
				</table>
				<br/>
				<table style="width: 100%;">
            <tr>
                
                                <td style="text-align: left;    width: 100%"><font style="font-size: 10px; text-align: left">Tanggal : <?php echo date('d - M - Y'); ?>
								<br/>Dibuat Oleh : <?php echo $sesen_username ?></font></td>
                
            </tr>
				</table>
		

		<br/>


		<table class="tabel2" align="center">
		  <thead>
			
		    
				<tr>
						<th style="text-align: center;"><font style="font-size: 10px; text-align: center">No</font></th>
                        <th style="text-align: center;"><font style="font-size: 10px; text-align: center">Petani</font></th>
                        <th style="text-align: center;"><font style="font-size: 10px; text-align: center">Alamat</font></th>
                        <th style="text-align: center;"><font style="font-size: 10px; text-align: center">Nama Hasil Perkebunan</font></th>
                        <th style="text-align: center;"><font style="font-size: 10px; text-align: center">Kategori</font></th>
						<th style="text-align: center;"><font style="font-size: 10px; text-align: center">Tanggal</font></th>
						<th style="text-align: center;"><font style="font-size: 10px; text-align: center">Hasil</font></th>
                </tr>
			</thead>
			<tbody>
			
				<?php 
			

				$id_petani = $_GET['id_petani'];
				$tahun  = $_GET['tahun'];
				$id_kategori  = $_GET['id_kategori'];
				
			$query1        = "SELECT *
							  FROM tb_hasil_perkebunan a JOIN tb_petani b ON a.id_petani = b.id_petani 
							  JOIN tb_alamat d ON b.id_alamat = d.id_alamat
							  JOIN tb_kategori c ON a.id_kategori = c.id_kategori 
							  WHERE a.id_kategori = '$id_kategori' AND a.id_petani = '$id_petani' AND YEAR(a.tanggal_post) = '$tahun'";
			$hasil1        = mysqli_query($conn,$query1);
				  
if(mysqli_num_rows($hasil1) == 0)
{die ("<script>alert('Data yang Anda cari tidak ditemukan');location.replace('$base_url')</script>");}
else{
		$i   = 1;
		$total = 0;
        while ($data_invoice = mysqli_fetch_array($hasil1))
        {
			$hasil = $data_invoice['luas_lahan'] * $data_invoice['hasil_per_lahan'];	
        ?>
				<tr>
                    <td style="text-align: center;    width: 2%"><font style="font-size: 10px; text-align: center"><?php echo $i ?></font></td>
                    <td style="text-align: center;    width: 10%"><font style="font-size: 10px; text-align: center"><?php echo $data_invoice['nama_petani']; ?></font></td>
                       <td style="text-align: center;    width: 25%"><font style="font-size: 10px; text-align: center"><?php echo $data_invoice['alamat']; ?></font></td>
                    <td style="text-align: center;    width: 20%"><font style="font-size: 10px; text-align: center"><?php echo $data_invoice['nama_hasil_perkebunan']; ?></font></td>
                    <td style="text-align: center;    width: 10%"><font style="font-size: 10px; text-align: center"><?php echo $data_invoice['nama_kategori']; ?></font></td>
					<td style="text-align: center;    width: 15%"><font style="font-size: 10px; text-align: center"><?php echo $data_invoice['tanggal_post']; ?></font></td>
					<td style="text-align: center;    width: 10%"><font style="font-size: 10px; text-align: center"><?php echo $hasil ?> TON</font></td>
				</tr>	
		      
				
		<?php $i++; 
	 $total += $hasil;}  ?>
				
		<tr>
                    <td colspan="6" align="right"><b><font style="font-size: 10px; text-align: left">TOTAL</font></b></td>
                   
					<td style="text-align: center;    width: 10%"><b><font style="font-size: 10px; text-align: center"><?php echo $total?> TON</font></b></td>
				</tr>
				

		  </tbody>
		</table>
				<?php } ?>
        
				
		

				
<br/><br/><br/><br/>
				<table style="width: 100%;">
            <tr>
                <td style="text-align: center;    width: 75%"></td>
								<td style="text-align: center;    width: 25%"><font style="font-size: 10px; text-align: center"> Mengetahui, <br/> Camat Tanjung Raja <br/><br/><br/><br/><br/><br/> HERI YANTO,SE.MM</font></td>
                
            </tr>
				</table>
	</body>
</html><!-- Akhir halaman HTML yang akan di konvert -->

<?php
// ob_get_clean = salah 1 fungsi dalam PHP
$content = ob_get_clean();
// Memanggil class HTML2PDF dari direktori html2pdf pada project kita
include '../html2pdf/html2pdf.class.php';
try
{
  // Mengatur invoice dalam format HTML2PDF
  // Keterangan: L = Landscape/ P = Portrait, A4 = ukuran kertas, en = bahasa, false = kode HTML2PDF, UTF-8 = metode pengkodean karakter
  $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'UTF-8', array(10, 5, 10, 0));
  // Mengatur invoice dalam posisi full page
  $html2pdf->pdf->SetDisplayMode('fullpage');
  // Menuliskan bagian content menjadi format HTML
  $html2pdf->writeHTML($content);
  // Mencetak nama file invoice
  $html2pdf->Output('laporan-hasil-perkebunan.pdf');
}
// Kodingan HTML2PDF
catch(HTML2PDF_exception $e)
{
  echo $e;
  exit;
}
?>
