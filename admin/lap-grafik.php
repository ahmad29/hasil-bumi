<?php 
  include "layout/header-form.php";

if(isset($_POST['simpan']))
{   
    $nama_petani    = mysqli_real_escape_string($conn, $_POST['nama_petani']);
    $email    = mysqli_real_escape_string($conn, $_POST['email']);
    $username    = mysqli_real_escape_string($conn, $_POST['username']);
    $password    = mysqli_real_escape_string($conn, $_POST['password']);
    $alamat    = mysqli_real_escape_string($conn, $_POST['alamat']);
    $no_hp    = mysqli_real_escape_string($conn, $_POST['no_hp']);
    $no_ktp    = mysqli_real_escape_string($conn, $_POST['no_ktp']);
    $status    = mysqli_real_escape_string($conn, $_POST['status']);

    $sql = "INSERT INTO tb_petani VALUES ('','$nama_petani','$email','$username','$password','$alamat',
            '$no_hp','$no_ktp','$status',now())";
    
    if(mysqli_query($conn, $sql)){
          echo "<script>location.replace('data-petani.php?tambah=true')</script>";
    }else{
        echo "Error updating record: " . mysqli_error($conn);
    }
}
?>

 
<div class="wrapper row-offcanvas row-offcanvas-left">
<?php 
  include "sidebar.php";
?> 
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--section starts-->
        <h1>
            Form Petani
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">
                    <i class="fa fa-fw ti-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">Tambah Petani</a>
            </li>
            
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <div class="box">
          <div class="box-body table-responsive padding">
          <form method="post" enctype="multipart/form-data">
                      <div class="col-xs-2"><label> Pilih Tahun</label>
                      <input class="form-control" name="dari" type="number" value="2019" min="2000" max='<?php echo date("Y"); ?>'/>
                        
                        
                      </div>
                      <div class="col-xs-2  "><label> </label>
                      <button type="submit" name="periode" class="form-control btn btn-success"> Proses</button>
                      </div>
          </form>
          </div>
        </div>
        <?php  
						if (isset($_POST['periode'])) {
              $dari = $_POST['dari'];
              ;
          $query1        = "SELECT COUNT(*) AS total, MONTH(tanggal_post) AS bulan FROM tb_hasil_perkebunan GROUP BY MONTH(tanggal_post)";
          $hasil1        = mysqli_query($conn,$query1);

          
          if(mysqli_num_rows($hasil1) == 0)
          {       echo "<div class='box'>
                        <div class='box-body table-responsive padding'>
                        <div class='col-xs-12'><label> Pada Tahun $dari Belum Ada Transaksi</label>
                        </div></div>";}
            
                  else
          {
            
					?>
	<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script type="text/javascript">
		$(function () {
			var chart;
			$(document).ready(function() {
				$.getJSON("dataline.php", function(json) {
				
					chart = new Highcharts.Chart({
						chart: {
							renderTo: 'container',
							type: 'line'
							
						},
						title: {
							text: 'Grafik Total '
							
						},
						subtitle: {
							text: '2019'
						
						},
						xAxis: {
							categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
						},
						yAxis: {
							title: {
								text: 'Total'
							},
							plotLines: [{
								value: 0,
								width: 1,
								color: '#808080'
							}]
						},
						tooltip: {
							formatter: function() {
									return '<b>'+ this.series.name +'</b><br/>'+
									this.x +': '+ this.y;
							}
						},
						legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'top',
							x: -10,
							y: 120,
							borderWidth: 0
						},
						series: json
					});
				});
			
			});
			
		});
		</script>
	<script src="<?php echo $base_url ?>assets/js/highcharts.js"></script>
	<script src="<?php echo $base_url ?>assets/js/exporting.js"></script>
        

<body>
<div class="box">
<div class="box-body">


	<div class="col-md-10" id="div1">
		<div class="panel panel-primary" align="center">
			<div class="panel-heading"> -- </div>
				<div class="panel-body">
					<div id ="container"></div>
				</div>
    </div>
	</div>
  <div class="col-md-10" >
  <div class='panel-heading' >
  <form method="post" enctype="multipart/form-data">
  <div class="col-xs-2  ">
  <button type="submit" name="cetak" class="form-control btn btn-success" onclick="printContent('div1')"><i class='fa fa-print'></i> Cetak</button>
  </div>
  </form>
</div></div>
</div></div>
  <?php } } ?>
<script src="<?php echo $base_url ?>assets/js/highcharts.js"></script>
<script>
	function printContent(el){
		var restorepage = document.body.innerHTML;
		var printcontent = document.getElementById(el).innerHTML;
		document.body.innerHTML = printcontent;
		window.print();
    window.close();
		document.body.innerHTML = restorepage;
	}
</script>


        </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->
</div>


<?php 
  include "layout/footer-form.php";
?>