<?php 
  include "layout/header-form.php";

    if(isset($_POST['update']))
    {   
        $id_kategori    = mysqli_real_escape_string($conn, $_POST['id_kategori']);
        $kategori    = mysqli_real_escape_string($conn, $_POST['kategori']);
        
        $sql = "UPDATE tb_kategori SET nama_kategori = '$kategori' WHERE id_kategori = $id_kategori";
        
        if(mysqli_query($conn, $sql)){
                echo "<script>location.replace('data-kategori.php?update=true')</script>";
        }else{
            echo "Error updating record: " . mysqli_error($conn);
        }
    }
  
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $sql = "SELECT * FROM tb_kategori WHERE id_kategori = $id";
        $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0){
                while ($data = mysqli_fetch_array($result)){
                    $id_kategori = $data['id_kategori'];
                    $nama_kategori = $data['nama_kategori']; 
                } 
            }
    }
?>

 
<div class="wrapper row-offcanvas row-offcanvas-left">
<?php 
  include "sidebar.php";
?> 
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--section starts-->
        <h1>
            Form Kategori
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">
                    <i class="fa fa-fw ti-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">Tambah Kategori</a>
            </li>
            
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-fw ti-move"></i> Form Tambah Kategori
                        </h3>
                        <span class="pull-right">
                            <i class="fa fa-fw ti-angle-up clickable"></i>
                            <i class="fa fa-fw ti-close removepanel clickable"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="#">
                            <div class="form-group">
                                <label for="input-text" class="col-sm-2 control-label">Kategori</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" 
                                           placeholder="Kategori" name="id_kategori" value="<?php echo $id_kategori; ?>">
                                    <input type="text" class="form-control" 
                                           placeholder="Kategori" name="kategori" value="<?php echo $nama_kategori; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        
                                    </label>
                                    <div class="col-sm-10 col-md-10">
                                    <button type="submit" name="update" class="btn btn-success"> Update</button>
                                    </div>
                                </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!--main content ends-->
        <div class="background-overlay"></div>
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->
</div>


<?php 

  include "layout/footer-form.php";
?>